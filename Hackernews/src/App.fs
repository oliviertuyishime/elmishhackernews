module App

open Elmish
open Feliz
open Thoth.Json
open Fable.SimpleHttp

type HackernewsItem = {
    id: int
    title: string
    url: string
    score : int
}

// json decoders
let itemDecoder: Decoder<HackernewsItem> = 
    Decode.object (fun fields -> {
        id = fields.Required.At ["id"] Decode.int
        title = fields.Required.At ["title"] Decode.string
        url = fields.Required.At ["url"] Decode.string
        score = fields.Required.At ["score"] Decode.int
    })

let parseItem json = 
    Decode.fromString itemDecoder json

// tabs
[<RequireQualifiedAccess>]
type Stories = 
    | New
    | Best
    | Top
    | Ask
    | Show
    | Job
 
type DeferredStoryItem = Deferred<Result<HackernewsItem, string>>

type State = {
    CurrentStories: Stories
    StoryItems: Deferred<Result<Map<int, DeferredStoryItem>, string>>
}

type Msg = 
    | LoadStoryItems of AsyncOperationEvent<Result<HackernewsItem list, string>>
    | ChangeStories of Stories

let init () =
    let initialState = { 
        CurrentStories = Stories.New
        StoryItems = HasNotStartedYet 
    }
    let initialCmd = Cmd.ofMsg (LoadStoryItems Started)
    initialState, initialCmd

let topStoriesEndpoint = "https://hacker-news.firebaseio.com/v0/topstories.json"
let storyItemEndpoint (itemId: int) = sprintf "https://hacker-news.firebaseio.com/v0/item/%d.json" itemId

let (|HttpOk|HttpError|) = 
    function
    | 200 -> HttpOk
    | _ -> HttpError

let storiesEndpoint stories =
    let fromBaseUrl = sprintf "https://hacker-news.firebaseio.com/v0/%sstories.json"
    match stories with
    | Stories.Ask -> fromBaseUrl "ask"
    | Stories.Best -> fromBaseUrl "best"
    | Stories.Top -> fromBaseUrl "top"
    | Stories.New -> fromBaseUrl "new"
    | Stories.Show -> fromBaseUrl "show"
    | Stories.Job -> fromBaseUrl "job"

let loadStoryItem itemId = async {
    let! (statusCode, responseText) = Http.get (storyItemEndpoint itemId)
    match statusCode with 
    | HttpOk -> 
        match (parseItem responseText) with 
        | Ok item -> return Some item
        | Error error -> return None 
    | HttpError -> return None
}

let loadStoryItems = async {
    do! Async.Sleep 1000
    let! (statusCode, responseText) = Http.get topStoriesEndpoint
    match statusCode with 
    | HttpOk -> 
        match Decode.fromString (Decode.list Decode.int) responseText with 
        | Ok ids -> 
            let! storyItems = 
                ids 
                |> List.truncate 10
                |> List.map loadStoryItem
                |> Async.Parallel
                |> Async.map (Array.choose id >> List.ofArray)
            return LoadStoryItems (Finished (Ok storyItems))             
        | Error parseError -> return LoadStoryItems (Finished (Error parseError))
    | HttpError -> 
        return LoadStoryItems (Finished (Error responseText))
}

let update (msg: Msg) (state: State) = 
    match msg with
    | LoadStoryItems Started -> { state with StoryItems = InProgress }, Cmd.fromAsync loadStoryItems
    | LoadStoryItems (Finished result) -> { state with StoryItems = Resolved result }, Cmd.none
    | ChangeStories stories -> state, Cmd.none

let storiesName = function
    | Stories.New -> "New"
    | Stories.Ask -> "Ask"
    | Stories.Best -> "Best"
    | Stories.Job -> "Job"
    | Stories.Top -> "Top"
    | Stories.Show -> "Show"

let renderTab currentStories stories dispatch =
    Html.a [ 
        prop.classes [ "nav-item"; "nav-link"]
        prop.className [ currentStories = stories, "active"]
        prop.onClick (fun _ -> if (currentStories <> stories) then dispatch (ChangeStories stories))
        prop.children [
            Html.span (storiesName stories) 
        ]
    ] 

let stories = [
    Stories.New
    Stories.Ask 
    Stories.Best
    Stories.Job
    Stories.Top
    Stories.Show
]

let renderTabs currentStories dispatch =
    Html.nav [
        prop.classes ["nav"; "nav-pills"; "nav-fill"]
        prop.children [
            for story in stories -> renderTab currentStories story dispatch
        ]
    ]

let renderError (errorMsg: string) =
    Html.h1 [
        prop.style [ style.color.red ]
        prop.text errorMsg
    ]

let div (classes: string list) (children: ReactElement list) =
    Html.div [
        prop.className classes
        prop.children children
    ]

let renderItem item = 
    Html.div [
        prop.key item.id
        prop.className "box"
        prop.style [ 
            style.marginTop 15 
            style.marginBottom 15
            style.padding 25
            style.borderRadius 5
            style.borderColor "#eee" 
            style.borderWidth 1 
            style.borderStyle.solid 
            style.boxShadow(0, 1, 1, 0, "#ddd")
        ]
        prop.children [
            Html.a [
                prop.style [ style.textDecoration.underline ]
                prop.target.blank
                prop.href item.url
                prop.text item.title
            ]
        ]
    ]

let spinner = 
    Html.div [
        prop.style [ style.textAlign.center; style.marginTop 20 ]
        prop.children [
            Html.i [
                prop.className "fa fa-cog fa-spin fa-2x"
            ]
        ]
    ]

let renderItems = function
    | HasNotStartedYet -> Html.none
    | InProgress -> spinner
    | Resolved (Error errorMsg) -> renderError errorMsg
    | Resolved (Ok items) -> React.fragment [ for item in items -> renderItem item ]

let render (state: State) (dispatch: Msg -> unit) =
    Html.div [
        prop.style [ style.padding 20 ]
        prop.children [
            Html.h1 [
                prop.className "title"
                prop.text "Elmish Hackernews"
            ]
            
            renderItems state.StoryItems
        ]
    ]